# What's here
In questa cartella sono raccolti alcuni script python per l'acquisizione e il salvataggio di forme d'onda

- [GUI.py](./GUI.py) è l'interfaccia grafica che permette di connettersi allo strumento, acquisire e salvare forme d'onda
- [CLI.py](./CLI.py) è la versione del programma da linea di comando
- [osciLib.py](./osciLib.py) è la raccolta di tutte le funzioni necessarie alla lettura e processamento
- [leggiOsci.ipynb](./leggiOsci.ipynb) è il primo esempio che venne prodotto nel 2021 di un quadernetto che mostra in realtà tutte le funzionalità
- [data](./data/) è la cartella di default in cui verranno salvati i dati. Contiene anche un [esempio di file di dati](./data/data.dat).