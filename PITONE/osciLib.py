"""
This file will host the functions for reading a TEKTRONIX TDS 2024C oscilloscope
"""

import usbtmc
import numpy as np
import matplotlib.pyplot as plt
import globalVars
from datetime import datetime
import inspect



def initOsci(idx):
    # global globalVars.dev
    try:
        myItem = usbtmc.list_devices()[idx]
        globalVars.dev =  usbtmc.Instrument(myItem.idVendor, myItem.idProduct)
        
        # Check if everything is correct
        checkIfAlive()
    except Exception as e:
        print(e)
        print (f"Current function: {inspect.stack()[0][3]}")
        print (f"Caller name: {inspect.stack()[1][3]}")


def checkIfAlive():
    # Check if everything is correct
    if globalVars.dev is not None:
        try: 
            print(globalVars.dev.ask("*IDN ?"))
        except Exception as e:
            print(e)
            print (f"Current function: {inspect.stack()[0][3]}")
            print (f"Caller name: {inspect.stack()[1][3]}")





# ================================================================
# Vecchie routine - Copiate da Jupyter


def acquisisciTot(filePath):
    plt.close("all")
    
    print(globalVars.dev.ask("*IDN?"))
    globalVars.dev.write("DATA:ENC ASCII\n")
    globalVars.dev.write("DATA:WIDTH 1\n")
    globalVars.dev.write("DATA:START 1\n")
    globalVars.dev.write("DATA:STOP 2500\n")

    # Leggo i due canali
    _, _, d1, dict1 = acquisisciChan(1)
    _, _, d2, dict2 = acquisisciChan(2)
    
    globalVars.dev.write("ACQ:STATE RUN\n")
    
    
    
    # Ottengo le 5 info
    xincr = float(dict1["XINCR"])
    yoff1 = float(dict1["YOFF"])
    yoff2 = float(dict2["YOFF"])
    ymult1 = float(dict1["YMULT"])
    ymult2 = float(dict2["YMULT"])
    
    
    
    # Costruisco i vettori con i dati
    time = np.linspace(0.5, 2500.5, 2500) * xincr
    v1 = (d1 - yoff1) * ymult1
    v2= (d2 - yoff2) * ymult2 
    
    
    
    # Preparo i dati da salvare
    matrice = np.concatenate((time[:,np.newaxis], v1[:,np.newaxis], v2[:,np.newaxis], ), axis = 1)
    np.savetxt(filePath, matrice, delimiter = ",")
    
    print("Dati salvati")
    
    
    
    # Plotto
    fig, ax = plt.subplots()
    ax.plot(time, v1, label = "Ch 1")
    ax.plot(time, v2, label = "Ch 2")
    
    ax.legend(fontsize = 14)
    ax.set_xlabel("Tempo (s)", fontsize = 14)
    ax.set_ylabel("Ampiezza (V)", fontsize = 14)
    ax.set_title("Plot", fontsize = 16)
    ax.grid(True)
    
    plt.show()
    
    
    print("Finito")
   

def acquisisciChan(i):
    print(f"Sto per acquisire il canale {i}")
    globalVars.dev.write(f"SEL:CH{i} ON\n")
    globalVars.dev.write("DATA:ENC ASCII\n")
    globalVars.dev.write(f"DATA:SOURCE CH{i}\n")
    globalVars.dev.write("VERBOSE ON\n")
    globalVars.dev.ask("DATA:START?\n")
    globalVars.dev.ask("DATA:STOP?\n")
    preambolo = globalVars.dev.ask("WFMPRE?\n")
    print(preambolo)
    globalVars.dev.write("ACQ:STATE STOP\n")
    datiraw = globalVars.dev.ask("CURVE?\n")
    print(datiraw)
    

    # Processo il preambolo
    tmpDict = dict(i.split(" ",1) for i in preambolo.split(";"))
    
    # processo i dati
    dati = np.array(datiraw[6:].split(","), dtype = float)

    
    return(datiraw, preambolo, dati, tmpDict)




# ================================================================
# Nuove routine fatte meglio

def acquireSomeChannels(filePath, ch = [True, True, False, False], 
                        showPlot = True, verbose=True):

    # Close if some window is open
    plt.close("all")

    # Global settings for acquisition
    globalVars.dev.write("DATA:ENC ASCII\n")
    globalVars.dev.write("DATA:WIDTH 1\n")
    globalVars.dev.write("DATA:START 1\n")
    globalVars.dev.write("DATA:STOP 2500\n")
    
    # Stop before reading
    globalVars.dev.write("ACQ:STATE STOP\n")

    # //READ SINGLE CHANNELS HERE
    vVect = []
    for i in range(len(ch)):
        if ch[i]:
            
            # Acquire data
            _, _, tmpData, tmpDdict = acquireSingleChannel(i, verbose=verbose)
            
            # Extract relevant informations
            xincr = float(tmpDdict["XINCR"])
            yoff = float(tmpDdict["YOFF"])
            ymult = float(tmpDdict["YMULT"])
            
            # Compute actual V
            v = (tmpData - yoff) * ymult
            vVect.append(v.copy())
    
    # Restart after reading
    globalVars.dev.write("ACQ:STATE RUN\n")

    
    # The time is in common among all the channels
    # Can use the last xincr
    time = np.linspace(0.5, 2500.5, 2500) * xincr

    # Preparo i dati da salvare
    matrice = np.concatenate((time[:,np.newaxis], *[vVect[i][:,np.newaxis] for i in range(len(ch)) if ch[i]] ), axis = 1)
    myHeader = f"{datetime.now().strftime('%Y-%m-%d %H:%M:%S')}\n{[i for i in range(len(ch)) if ch[i]]}"
    np.savetxt(filePath, matrice, delimiter = ",", header=myHeader, comments="#")
    
    print("Dati salvati")
    
    
    
    # Plotto
    if showPlot:
        fig, ax = plt.subplots()

        for i in range(len(ch)):
            if ch[i]:
                ax.plot(time, vVect[i], label = f"Ch {i+1}")
        
        ax.legend(fontsize = 14)
        ax.set_xlabel("Tempo (s)", fontsize = 14)
        ax.set_ylabel("Ampiezza (V)", fontsize = 14)
        ax.set_title("Plot", fontsize = 16)
        ax.grid(True)
        
        plt.show()
    
    
    print("Finito")
    
    
    
    
def acquireSingleChannel(i, verbose=True):
    """acquireSingleChannel Acquires a single channel 

    Args:
        i (int): Channel to be acquired
    """
    print(f"Sto per acquisire il canale {i}")
    
    # Configuring readout of a given channel
    # I am not even sure everything is required
    
    globalVars.dev.write(f"SEL:CH{i+1} ON\n")
    globalVars.dev.write("DATA:ENC ASCII\n")
    globalVars.dev.write(f"DATA:SOURCE CH{i+1}\n")
    globalVars.dev.write("VERBOSE ON\n")
    
    # Visto che chiedo senza intercettare, probabilmente non servono
    # globalVars.dev.ask("DATA:START?\n")
    # globalVars.dev.ask("DATA:STOP?\n")
    
    preambolo = globalVars.dev.ask("WFMPRE?\n")
    if verbose: print(preambolo)
    
    datiraw = globalVars.dev.ask("CURVE?\n")
    if verbose: print(datiraw)
    

    # Processo il preambolo
    tmpDict = dict(i.split(" ",1) for i in preambolo.split(";"))
    
    # processo i dati
    # Rimuovo :CURVE e separo alle virgole
    dati = np.array(datiraw[6:].split(","), dtype = int)

    
    return(datiraw, preambolo, dati, tmpDict)
