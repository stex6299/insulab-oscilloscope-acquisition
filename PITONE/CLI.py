#!/usr/bin/env python3

import argparse
from osciLib import *
import globalVars

import sys, os, re, glob


parser = argparse.ArgumentParser(
    prog="Insulab Oscilloscope Acquisition Software",
    description="CLI for acquiring the goniometer",
    epilog="Text at the bottom of help",
)

# Output file
parser.add_argument(
    "-o",
    "--output-file",
    "--of",
    dest="outputFile",
    required=True,
    help="Path for the output file",
)

# Force overwrite
parser.add_argument(
    "-f",
    "--force-overwrite",
    "--fo",
    dest="forceOverwrite",
    action="store_true",
    help="Decide wether to force the overwriting of existing file",
)


parser.add_argument(
    "-n",
    "--device-number",
    "--npts",
    dest="id",
    type=int,
    default=0,
    help="id of the device",
)


# Read Channels
parser.add_argument(
    "-1",
    dest="Chan1",
    action="store_true",
    help="Read channel 1",
)
parser.add_argument(
    "-2",
    dest="Chan2",
    action="store_true",
    help="Read channel 2",
)
parser.add_argument(
    "-3",
    dest="Chan3",
    action="store_true",
    help="Read channel 3",
)
parser.add_argument(
    "-4",
    dest="Chan4",
    action="store_true",
    help="Read channel 4",
)


# Verbose mode
parser.add_argument(
    "-v",
    "--verbose",
    dest="verbose",
    action="store_true",
    help="Verbose output",
)

# Show plot
parser.add_argument(
    "-p",
    "--plot",
    "--show-plot",
    dest="plot",
    action="store_true",
    help="Show the plot",
)




args = parser.parse_args()

channels = [args.Chan1, args.Chan2, args.Chan3, args.Chan4]

# Inizializo
initOsci(args.id)


fileName = os.path.join("./data", f"{args.outputFile}.dat")
if globalVars.dev is None:
    
    # No device inizializzato
    print("No device initialized")
    sys.exit(1)

if os.path.exists(fileName):
    if args.forceOverwrite:
        # acquisisciTot(fileName)
        acquireSomeChannels(fileName, channels, showPlot = args.plot, verbose=args.verbose)
        print(f"---> {fileName} overwritten")
    else:
        print("Cannot overwrite without -f flag")
else:
    # acquisisciTot(fileName)
    acquireSomeChannels(fileName, channels, showPlot = args.plot, verbose=args.verbose)
    print(f"---> {fileName} saved")
