#!/usr/bin/env python3 

from MainWindow import *
from PyQt5.QtCore import pyqtSlot

import sys, os, re, glob
from osciLib import *
import globalVars



class MainWindow(QtWidgets.QMainWindow, Ui_MainWindow):
    def __init__(self):
        super(MainWindow, self).__init__()
        self.setupUi(self)
        
        # Default both channels to be acquired
        self.box_ch1.setChecked(True)
        self.box_ch2.setChecked(True)
        self.box_ch3.setChecked(False)
        self.box_ch4.setChecked(False)
        # self.box_ch1.setDisabled (True)
        # self.box_ch2.setDisabled (True)
        
        self.lstCheckbox = [self.box_ch1, self.box_ch2, self.box_ch3, self.box_ch4]
        
        # Popoliamo la combobox con tutti i devices disponibili
        tmpLst = usbtmc.list_devices()
        try:
            for i in tmpLst: 
                self.combo_device.addItem(str(i).split("\n")[0].replace("=", ""))
                # print(i)
            # self.combo_device.setCurrentIndex(0)
            initOsci(0)
        except Exception as e:
            print(e)
            print (f"Current function: {inspect.stack()[0][3]}")
            print (f"Caller name: {inspect.stack()[1][3]}")

        
    # Viene cliccato un elemento della combobox
    @pyqtSlot()
    def on_combo_device_index_changed(self, index):
        print("Ciao")
        
        # // Not implemented yet
        
        # currTxt = self.combo_device.currentText()
        # print(f"Current index: {index}")
        # print(f"Current text: {currTxt}")
        
        # # So che il testo sarà
        # #<DEVICE ID 0699:03a6 on Bus 001 Address 012>
        # tmp = currTxt.split(" ")[2].split(":")
        
        # # Estraggo la rappresentazione esadecimale di ID venditore e ID modello
        # vend = tmp[0]
        # model = tmp[1]
        
        # # Interpreto le stringhe come numeri esadecimali
        # vendInt = int(vend, 16)
        # modelInt = int(model, 16)
        
        # self.dev =  usbtmc.Instrument(vendInt, modelInt)
        
        
    @pyqtSlot()
    def on_btn_save_clicked(self):
        
       
        
        
        # if(self.box_ch1.isChecked()):
        #     print("Canale 1")
        # if(self.box_ch2.isChecked()):
        #     print("Canale 2")
        
        checkBoxStates = [i.isChecked() for i in self.lstCheckbox]
        
        
        
        
        # Name of the output file
        fileName = os.path.join("./data", f"{self.txt_filename.text()}.dat")
        print(fileName)
        if fileName == "": return
        if globalVars.dev is None: return # No device inizializzato
        
        
        if os.path.exists(fileName):
            
            # MessageBox to protect overwriting
            qm = QtWidgets.QMessageBox
            ret = qm.warning(self,'DANGER OVERWRITING', "Are you sure to overwrite existing file?", qm.Yes | qm.No, defaultButton=qm.No)
            
            if ret == qm.Yes: 
                # acquisisciTot(fileName)
                acquireSomeChannels(fileName, checkBoxStates, showPlot = True, verbose=True)
                print(f"I have overwritten {fileName}")
        else:
            # acquisisciTot(fileName)
            acquireSomeChannels(fileName, checkBoxStates, showPlot = True, verbose=True)
            print(f"I have written {fileName}")
        


        
if __name__ == '__main__':
    app = QtWidgets.QApplication(sys.argv)
    app.setApplicationName("Insulab Oscilloscope Acquisition Software")

    mainWindow = MainWindow()
    mainWindow.show()
    sys.exit(app.exec_())
