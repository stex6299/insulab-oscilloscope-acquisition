# What's here
Lo scopo di questa repository è leggere un oscilloscopio **TEKTRONIX TDS 2024C** tramite USB.

La cartella [PITONE](./PITONE/) contiene software necessario all'acquisizione e salvataggio di forme d'onda, mentre in [ANALISI](./ANALISI/) si possono trovare esempi per l'apertura e visualizzazione dei dati.


# Setup the environment

## Prerequisiti windows
Installiamo i prerequisiti (`pyusb` e `libusb`) con
```bash
pip install pyusb libusb
```

## Prerequisiti Ubuntu/Debian
[Riferimento](https://github.com/python-ivi/python-usbtmc#configuring-udev)
Configuriamo *udev* per dare al sistema i corretti diritti per interfacciarsi con l'oscilloscopio

```bash
sudo -e /etc/udev/rules.d/usbtmc.rules
```

e pastiamo le seguenti righe, avendo cura di mettere dopo `ATTRS{idVendor}` e `ATTRS{idProduct}` i codici che possono essere ottenuti scrivendo in un terminale `lsusb | grep -ie "tektronix"` subito dopo ID (Un output potrebbe essere `Bus 002 Device 006: ID 0699:03a6 Tektronix, Inc. Tektronix TDS2024C`)

```raw
# USBTMC instruments

# Agilent MSO7104
SUBSYSTEMS=="usb", ACTION=="add", ATTRS{idVendor}=="0699", ATTRS{idProduct}=="03a6", GROUP="usbtmc", MODE="0660"

# Devices
KERNEL=="usbtmc/*",       MODE="0660", GROUP="usbtmc"
KERNEL=="usbtmc[0-9]*",   MODE="0660", GROUP="usbtmc"
```

Potrebbe essere necessario creare un gruppo `usbtmc` ed aggiungerci

```bash
sudo groupadd usbtmc
sudo usermod -a -G usbtmc insudaq
```

Riavviare il pc per confermare questa modifica

Sui computer del laboratorio in cui python è stato installato senza anaconda, per poter usare i comandi `pyuic5` e `pyrcc5` richiesti dal [Makefile](./PITONE/Makefile), è necessario aggiungere alla variabile d'ambiente `PATH` il percorso relativo. Il modo più economico consiste nell'aggiungere la seguente riga al `.bashrc`
```
# Scarsi 20231207 For having python binaries available (pyuic5 and pyrcc5)
export PATH="/home/insudaq/.local/bin":$PATH
```



## Prerequisiti comuni
### GUI
```bash
pip install qt pyqt
pip install pyqt5-tools
```

### Altre librerie usate
```bash
pip install numpy matplotlib
```


## Libreria `usbtmc`
Installiamo la libreria `usbtmc` per comunicare con l'oscilloscopio
```bash
git clone https://github.com/python-ivi/python-usbtmc.git && cd python-usbtmc/
python3 setup.py install
```



# Forward serial port on WLS
[Riferimento](https://github.com/dorssel/usbipd-win) e [mio sito](https://scarsi.web.cern.ch/guide/GuidaSerialWSL.html)
Breve recap per poter utilizzare una seriale, o anche un usbtmc device su WSL

## Lato Windows
- Aprire una Power Shell come amministratore
- Installare **usbipd** con `winget install usbipd`
- Scrivere il seguente comando per ottenere una lista delle porte disponibili
`usbipd wsl list`  
- Per associarla al wsl, è sucficiente scrivere
`usbipd wsl attach --busid=<BUSID>`  
Esempio concreto
`usbipd wsl attach --busid=4-3`

Nota: nel caso si abbiano più WSL installati, potrebbe essere necessario specificare la distribuzione col `-d <NAME>`. Il nome si può ottenere con `wsl -l`

## Lato WSL
- Runnare i due seguenti comandi. Del primo in realtà non sono sicuro che sia necessario. Comunque, se vuole un `--fix-missing`, metteteglielo
```bash
sudo apt install linux-tools-virtual hwdata
sudo update-alternatives --install /usr/local/bin/usbip usbip `ls /usr/lib/linux-tools/*/usbip | tail -n1` 20
```
(Da qui in poi non ho verificato che funzioni con WSL, ma copio dalla guida)
- Aprire ubuntu e scrivere `dmesg`
- Si dovrebbe vedere qualcosa che dice che il dispositivo USB è stato visto correttamente. In particolare, memorizzarsi il numerino dopo ttyUSB  
- Typare il seguente comando per configurare la porta seriale, avendo cura di sostituire al posto di USB0 quello scritto sopra 
`setserial -g -a /dev/ttyUSB0`
- Se non esistesse il comando setserial…
`sudo apt install setserial`



# Known bugs
- Often `checkIfAlive` prints `[Errno 13] Access denied (insufficient permissions)` even if it can initialize the device...

